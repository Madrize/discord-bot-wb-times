import discord
import asyncio

import calendar

from datetime import datetime, timedelta

from discord.ext.commands import Bot
from discord.ext import commands


# invite link : https://discordapp.com/oauth2/authorize?client_id=CLIENTID&scope=bot

"""

    Example usage #1;

        10m = 10 minutes
        1h = 1 hour

        > !which 10m
        > Emissary Justice in 5 minutes. 12.30 in game time, Spirit Valley

        > !which 8h
        > Emissary Nanoq in 5 hours and 20 minutes. 18.00 in game time, Golden Sandbar


    > !which monday

    > !listAll
    
    > !when Justice

    > !whenNext Justice

    > !which today

"""

APP_TOKEN = "NDI1MDE5OTEzOTI1NTU4Mjcy.DZJrfA.23vM7asRnvhG-Ye0A3tn-jMlYBI"

DAYS = [(day.lower(), index) for day, index in zip(calendar.day_name, range(7))]

client = discord.Client()

TIMEZONES = {
    "gametime": "In-Game Time",
    "gmt": "GMT"
}


EVENTS = {
    
    "monday" : [

        {
            "name": "Monday Boss X",
            "time": "12.15",
            "location": "Monday Location X",
            "timezone": "gametime"
        },

    ],
    
    "tuesday" : [

        {
            "name": "Tuesday Boss Y",
            "time": "12.25",
            "location": "Tuesday Location Y",
            "timezone": "gametime"
        },

    ],
    
    "wednesday" : [

        {
            "name": "Wednesday Boss Y",
            "time": "12.25",
            "location": "Wednesday Location Y",
            "timezone": "gametime"
        },

    ]

}



@client.event
async def on_ready():
    pass


@client.event
async def on_message(message):

    data_list = message.content.split(" ")

    if len(data_list) != 2:
        return

    command, params = data_list[0], data_list[1]

    events = []

    if command == "!today":
        pass

    elif command == "!which":

        if params == "today":
            events = filter_events_by_day(get_today())

        elif params in dict(DAYS).keys():
            events = filter_events_by_day(params)

    elif command == "!when":
        pass

    response = format_response(events)
    
    await client.send_message(message.channel, response)


def filter_events_by_day(day):
    return EVENTS[day]


def filter_events(amount, key):

    results = []

    now = datetime.now()

    for day, bosses in EVENTS.items():

        for boss_spawn in bosses:

            boss_name = boss_spawn["name"]
            spawn_time = boss_spawn["time"]

            event_time = event_date_to_datetime(day, spawn_time)

            time_diff = get_time_difference(amount, key)

            if now < event_time and (event_time - now < time_diff):
                results.append((boss_name, spawn_time))

    return results


def validate_message(day, key):

    days = [(day.lower(), index) for day, index in zip(calendar.day_name, range(7))]

    return day in days.keys()


def get_time_difference(amount, key):

    params = {
        "m": "minutes",
        "h": "hours"
    }

    return timedelta(**{params[key]:amount})


def get_today():
    today = datetime.today().weekday()
    for day, index in dict(DAYS).items():
        if index == today:
            return day
    return None


def event_date_to_datetime(day, time):
    pass


def format_response(results):

    resp = "Data:\n"

    for spawn_data in results:

        item = "%s will appear in %s, at %s" % (spawn_data["name"], spawn_data["location"], spawn_data["time"])
        resp += "%s\n" % item

    return resp


client.run(APP_TOKEN)
